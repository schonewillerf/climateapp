package hu.adsd.ClimateServer.projects;

import hu.adsd.ClimateServer.products.Product;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@RequestMapping( "/api" )
public class ProjectController
{
    private final ProjectRepository projectRepository;

    @GetMapping( "/project" )
    public Project getProject()
    {
        // Returns the first and only project from repository
        return projectRepository.findAll().get( 0 );
    }

    @PostMapping( "/project" )
    public Product addProduct( @RequestBody Product product )
    {
        // Returns the first and only project from repository
        Project project = projectRepository.findAll().get( 0 );
        project.addProduct( product );

        projectRepository.save( project );

        return product;
    }

    @PutMapping( "/project/{id}" )
    public Product updateProduct( @PathVariable int id, @RequestBody Product product )
    {
        // Returns the first and only project from repository
        Project project = projectRepository.findAll().get( 0 );
        project.updateProduct( product, id );

        projectRepository.save( project );

        return product;
    }

    @DeleteMapping( "/project/{id}" )
    public Product removeProduct( @PathVariable String id )
    {
        // Returns the first and only project from repository
        Project project = projectRepository.findAll().get( 0 );
        project.removeProduct( id );

        projectRepository.save( project );

        return new Product();
    }
}
