import {Product} from './Product';

export interface Project {
  id: string;
  products: Product[];
}
