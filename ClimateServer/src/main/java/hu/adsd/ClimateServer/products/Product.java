package hu.adsd.ClimateServer.products;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@NoArgsConstructor
public class Product
{
    @Id
    private String id;

    private String name;
    private int CarbonAmount;
    private CirculationType circulationType;

    public Product( String name, int carbonAmount, CirculationType circulationType )
    {
        this.name = name;
        CarbonAmount = carbonAmount;
        this.circulationType = circulationType;
    }
}
