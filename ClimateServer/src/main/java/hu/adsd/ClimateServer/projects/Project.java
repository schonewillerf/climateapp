package hu.adsd.ClimateServer.projects;

import hu.adsd.ClimateServer.products.Product;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

@Data
public class Project
{
    @Id
    private String id;

    private List<Product> products;

    public Project()
    {
        this.products = new ArrayList<>();
    }

    public void addProduct( Product product )
    {
        this.products.add( product );
    }

    public void updateProduct( Product product, int index )
    {
        this.products.set( index, product );
    }

    public void removeProduct( String id )
    {
        this.products.removeIf( product -> product.getId().equals( id ) );
    }
}
