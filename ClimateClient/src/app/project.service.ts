import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Product} from './Product';
import {Observable} from 'rxjs';
import {Project} from './Project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private projectUrl = 'http://localhost:8080/api/project';

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }

  addProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(this.projectUrl, product, this.httpOptions);
  }

  removeProduct(product: Product): Observable<Product> {
    const url = `${this.projectUrl}/${product.id}`;

    return this.http.delete<Product>(url, this.httpOptions);
  }


  getProject(): Observable<Project> {
    return this.http.get<Project>(this.projectUrl);
  }
}
