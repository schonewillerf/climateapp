import {Component, OnInit} from '@angular/core';
import {Project} from '../Project';
import {ProjectService} from '../project.service';
import {Product} from '../Product';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit {
  project: Project;
  carbonTotal = 0;

  constructor(private projectService: ProjectService) {
  }

  ngOnInit(): void {
    this.getProject();
  }

  private getProject(): void {
    this.projectService.getProject()
      .subscribe(fetchedProject => {
        this.project = fetchedProject;
        this.updateCarbonTotal();
      }
    );
  }

  private updateCarbonTotal(): void {
    let total = 0;
    this.project.products.map(item => total = total + item.carbonAmount);
    this.carbonTotal = total;
  }

  public remove( product: Product ): void {
    this.project.products = this.project.products.filter( p => p !== product);
    this.updateCarbonTotal();
    this.projectService.removeProduct( product ).subscribe();
  }
}
