import {Component, OnInit} from '@angular/core';
import {Product} from '../Product';
import {ProductService} from '../product.service';
import {ProjectService} from '../project.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: Product[];

  constructor(
    private productService: ProductService,
    private projectService: ProjectService
  ) { }

  ngOnInit(): void {
    this.getProducts();
  }

  private getProducts(): void {
    this.productService.getProducts()
      .subscribe( fetchedProducts => this.products = fetchedProducts );
  }

  add( product: Product ): void {
    this.projectService.addProduct( product )
      .subscribe( postedProduct => console.log(postedProduct));
  }
}
