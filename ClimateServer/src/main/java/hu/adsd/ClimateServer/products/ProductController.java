package hu.adsd.ClimateServer.products;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping( "/api" )
public class ProductController
{
    private final ProductRepository productRepository;

    @GetMapping( "/products" )
    public List<Product> getProducts()
    {
        return productRepository.findAll();
    }
}
