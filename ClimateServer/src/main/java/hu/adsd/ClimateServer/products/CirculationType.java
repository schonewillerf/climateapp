package hu.adsd.ClimateServer.products;

public enum CirculationType
{
    NEW,
    RECYCLED,
    REUSED
}
