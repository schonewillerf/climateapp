# Full Stack Prototype

This is a prototype for a full stack project that has seperate client/front-end and server/back-end.
The client in this project is realised with Angular and the server with Spring Boot Frameworks. 

## Usage

Inside the ClimateClient folder do:
```
npm install
ng serve
```
Inside the backend folder do:
```
./mvnw spring-boot:run
```

Visit http://localhost:8080/ to proceed to the Angular Client or Browse the API documentation
