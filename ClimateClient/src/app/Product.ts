export interface Product {
  id: string;
  name: string;
  circulationType: string;
  carbonAmount: number;
}
