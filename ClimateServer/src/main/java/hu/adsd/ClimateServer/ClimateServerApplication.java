package hu.adsd.ClimateServer;

import hu.adsd.ClimateServer.products.CirculationType;
import hu.adsd.ClimateServer.products.Product;
import hu.adsd.ClimateServer.products.ProductRepository;
import hu.adsd.ClimateServer.projects.Project;
import hu.adsd.ClimateServer.projects.ProjectRepository;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@AllArgsConstructor
@SpringBootApplication
public class ClimateServerApplication implements CommandLineRunner
{
    private final ProductRepository productRepository;
    private final ProjectRepository projectRepository;

    public static void main( String[] args )
    {
        SpringApplication.run( ClimateServerApplication.class, args );
    }

    @Override
    public void run( String... args ) throws Exception
    {
        projectRepository.deleteAll();
        productRepository.deleteAll();

        productRepository.save( new Product( "Kraan", 300, CirculationType.NEW ) );
        productRepository.save( new Product( "Kraan", 88, CirculationType.RECYCLED ) );
        productRepository.save( new Product( "Kraan", 0, CirculationType.REUSED ) );
        productRepository.save( new Product( "Toilet Hoog", 148, CirculationType.NEW ) );
        productRepository.save( new Product( "Toilet Hoog", 12, CirculationType.RECYCLED ) );
        productRepository.save( new Product( "Toilet Hoog", 0, CirculationType.REUSED ) );
        productRepository.save( new Product( "Toilet Laag", 118, CirculationType.NEW ) );
        productRepository.save( new Product( "Toilet Laag", 10, CirculationType.RECYCLED ) );
        productRepository.save( new Product( "Toilet Laag", 0, CirculationType.REUSED ) );
        productRepository.save( new Product( "Wasbak", 283, CirculationType.NEW ) );
        productRepository.save( new Product( "Wasbak", 16, CirculationType.RECYCLED ) );
        productRepository.save( new Product( "Wasbak", 0, CirculationType.REUSED ) );

        projectRepository.save( new Project() );
    }
}
